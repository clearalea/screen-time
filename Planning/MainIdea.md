I want to create an app that will genuinely help me have more control over how I use my phone.

This app should build up from the screen time settings already in a phone, rather than undermine them or go around them.

I am creating this in XCode using Swift and the Screeen Time API for iPhones.


When the time limit for an app has been reached, a screen should pop up, allowing the user to close the app or add a short extension to finish something they were in the middle of.

after that time is up, the app should force the user to take a break (default 5 minutes? - this is something a user should be able to adjust). After that time is up, the user is then allowed to expand the time, up to an hour at most.


FIGMA LINK:
https://www.figma.com/design/dXMwxAG250lvpfxrZRgaf7/PhoneKiller?node-id=0-1&t=5f6UK45lGU4nwCfZ-0
