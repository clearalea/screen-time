//
//  PhoneKillerApp.swift
//  PhoneKiller
//
//  Created by Clara Durling on 6/15/24.
//

import SwiftUI

@main
struct PhoneKillerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
